﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

namespace Tacticsoft
{
	/// <summary>
	/// Base class for Horizontal and Vertical Table Views
	/// </summary>
    [RequireComponent(typeof(ScrollRect))]
    public abstract class TableViewBase : MonoBehaviour
    {

        #region Public API
        /// <summary>
        /// The data source that will feed this table view with information. Required.
        /// </summary>
		public ITableViewDataSource dataSource
        {
            get { return m_dataSource; }
			set { m_dataSource = value; m_requiresReload = true; }
        }
        
        [System.Serializable]
        public class CellVisibilityChangeEvent : UnityEvent<int, bool> { }
        /// <summary>
        /// This event will be called when a cell's visibility changes
        /// First param (int) is the cell index, second param (bool) is whether or not it is visible
        /// </summary>
        public CellVisibilityChangeEvent onCellVisibilityChanged;

        /// <summary>
        /// Get a cell that is no longer in use for reusing
        /// </summary>
        /// <param name="reuseIdentifier">The identifier for the cell type</param>
        /// <returns>A prepared cell if available, null if none</returns>
        public TableViewCell GetReusableCell(string reuseIdentifier)
		{
            LinkedList<TableViewCell> cells;
            if (!m_reusableCells.TryGetValue(reuseIdentifier, out cells)) {
                return null;
            }
            if (cells.Count == 0) {
                return null;
            }
            TableViewCell cell = cells.First.Value;
            cells.RemoveFirst();
            return cell;
        }

        public bool isEmpty { get; set; }

        /// <summary>
        /// Reload the table view. Manually call this if the data source changed in a way that alters the basic layout
		/// (number of items changed, etc)
        /// </summary>
        public void ReloadData()
		{
			m_cellSizes = new Vector2[m_dataSource.GetNumberOfCellsForTableView(this)];
			this.isEmpty = m_cellSizes.Length == 0;
            if (this.isEmpty) {
                ClearAllCells();
                return;
            }
			m_cumulativeCellHeights = new float[m_cellSizes.Length];
			m_cumulativeCellWidths = new float[m_cellSizes.Length];

            m_cleanCumulativeIndex = -1;

			for (int i = 0; i < m_cellSizes.Length; i++) {
				m_cellSizes[i] = m_dataSource.GetSizeForCellInTableView(this, i);
            }

			m_scrollRect.content.sizeDelta = new Vector2(CalculateContentWidthToIndex(m_cellSizes.Length - 1), 
			                                             CalculateContentHeightToIndex(m_cellSizes.Length - 1));

            RecalculateVisibleCellsFromScratch();
            m_requiresReload = false;
        }

        /// <summary>
		/// Get cell at a specific index (if active). Returns null if not.
        /// </summary>
        public TableViewCell GetCellAtIndex(int index)
        {
            TableViewCell retVal = null;
			m_visibleCells.TryGetValue(index, out retVal);
            return retVal;
        }

        /// <summary>
        /// Get the range of the currently visible cells
        /// </summary>
        public Range visibleCellRange 
		{
            get { return m_visibleCellRange; }
        }

        /// <summary>
        /// Notify the table view that one of its cells changed size
        /// </summary>
        public void NotifyCellDimensionsChanged(int index)
		{
			Vector2 oldSize = m_cellSizes[index];

			m_cellSizes[index] = m_dataSource.GetSizeForCellInTableView(this, index);
            m_cleanCumulativeIndex = Mathf.Min(m_cleanCumulativeIndex, index - 1);

			Vector2 cellSize = m_cellSizes[index];
            if (m_visibleCellRange.Contains(index)) {
				TableViewCell cell = GetCellAtIndex(index);
                cell.GetComponent<LayoutElement>().preferredHeight = cellSize.y;
				cell.GetComponent<LayoutElement>().preferredWidth = cellSize.x;
            }

			float heightDelta = cellSize.y - oldSize.y;
			float widthDelta = cellSize.x - oldSize.x;

            m_scrollRect.content.sizeDelta = new Vector2(m_scrollRect.content.sizeDelta.x + widthDelta,
                m_scrollRect.content.sizeDelta.y + heightDelta);
            m_requiresRefresh = true;
        }

        /// <summary>
        /// Get the maximum scrollable height of the table. scrollY property will never be more than this.
        /// </summary>
        public float scrollableHeight
		{
            get {
                return m_scrollRect.content.rect.height - (this.transform as RectTransform).rect.height;
            }
        }

		/// <summary>
		/// Get the maximum scrollable width of the table. scrollX property will never be more than this.
		/// </summary>
		public float scrollableWidth
		{
			get {
				return m_scrollRect.content.rect.width - (this.transform as RectTransform).rect.width;
			}
		}

		/// <summary>
		/// Gets the height of the table.
		/// </summary>
		/// <value>The height of the table.</value>
		public float TableContentHeight
		{
			get {
				return m_scrollRect.content.sizeDelta.y;
			}
		}

		/// <summary>
		/// Gets the width of the table.
		/// </summary>
		/// <value>The width of the table.</value>
		public float TableContentWidth
		{
			get {
				return m_scrollRect.content.sizeDelta.x;
			}
		}

        #endregion

        #region protected implementation

        protected ITableViewDataSource m_dataSource;
        protected bool m_requiresReload;

        protected ScrollRect m_scrollRect;

        protected Vector2[] m_cellSizes;
        protected float[] m_cumulativeCellHeights;
		protected float[] m_cumulativeCellWidths;

        protected int m_cleanCumulativeIndex;

        protected Dictionary<int, TableViewCell> m_visibleCells;
        protected Range m_visibleCellRange;

        protected RectTransform m_reusableCellContainer;
        protected Dictionary<string, LinkedList<TableViewCell>> m_reusableCells;

        protected float m_scrollY;
		protected float m_scrollX;

        protected bool m_requiresRefresh;

		protected enum CellRange {FirstCell, LastCell};

        protected void ScrollViewValueChanged(Vector2 newScrollValue)
		{
            float relativeScrollY = 1f - newScrollValue.y;
            m_scrollY = relativeScrollY * scrollableHeight;

			float relativeScrollX = newScrollValue.x;
			m_scrollX = relativeScrollX * scrollableWidth;

            m_requiresRefresh = true;
        }

        protected void RecalculateVisibleCellsFromScratch()
		{
            ClearAllCells();
            SetInitialVisibleCells();
        }

        protected void ClearAllCells()
		{
            while (m_visibleCells.Count > 0) {
				HideCell(CellRange.FirstCell);
            }
            m_visibleCellRange = new Range(0, 0);
        }

        protected virtual void Awake()
        {
            m_scrollRect = GetComponent<ScrollRect>();
        
            m_visibleCells = new Dictionary<int, TableViewCell>();

            m_reusableCellContainer = new GameObject("ReusableCells", typeof(RectTransform)).GetComponent<RectTransform>();
            m_reusableCellContainer.SetParent(this.transform, false);
            m_reusableCellContainer.gameObject.SetActive(false);
            m_reusableCells = new Dictionary<string, LinkedList<TableViewCell>>();
        }
        
        void Update()
        {
            if (m_requiresReload) {
                ReloadData();
            }
        }

        void LateUpdate()
		{
            if (m_requiresRefresh) {
                RefreshVisibleCells();
            }
        }

        void OnEnable()
		{
            m_scrollRect.onValueChanged.AddListener(ScrollViewValueChanged);
        }

        void OnDisable()
		{
            m_scrollRect.onValueChanged.RemoveListener(ScrollViewValueChanged);
        }
        
        protected abstract Range CalculateCurrentVisibleCellRange();

        protected void SetInitialVisibleCells()
        {
            Range visibleCells = CalculateCurrentVisibleCellRange();
            for (int i = 0; i < visibleCells.count; i++) {
                AddCell(visibleCells.from + i, CellRange.LastCell);
            }
            m_visibleCellRange = visibleCells;
            UpdateScrollRectDynamicFillerElements();
        }

        protected void AddCell(int index, CellRange position)
        {
            if (index > m_cellSizes.Length)
            {
                Debug.DebugBreak();
            }

			TableViewCell newCell = m_dataSource.GetCellForIndexInTableView(this, index);
            newCell.transform.SetParent(m_scrollRect.content, false);

            LayoutElement layoutElement = newCell.GetComponent<LayoutElement>();
            if (layoutElement == null) {
                layoutElement = newCell.gameObject.AddComponent<LayoutElement>();
            }

			Vector2 cellSize = m_cellSizes[index];
            layoutElement.preferredHeight = cellSize.y;
			layoutElement.preferredWidth = cellSize.x;
            
            m_visibleCells[index] = newCell;
            if (position == CellRange.LastCell) {
                newCell.transform.SetSiblingIndex(m_scrollRect.content.childCount - 2); //One before bottom ScrollRectDynamicFiller
            } else {
                newCell.transform.SetSiblingIndex(1); //One after the top ScrollRectDynamicFiller
            }
            this.onCellVisibilityChanged.Invoke(index, true);
        }

        protected void RefreshVisibleCells()
        {
            Range newVisibleCells = CalculateCurrentVisibleCellRange();
            int oldTo = m_visibleCellRange.Last();
            int newTo = newVisibleCells.Last();

            if (newVisibleCells.from > oldTo || newTo < m_visibleCellRange.from) {
                //We jumped to a completely different segment this frame, destroy all and recreate
				RecalculateVisibleCellsFromScratch();
                m_requiresRefresh = false;
                return;
            }

            //Remove cells that disappeared to the top
            for (int i = m_visibleCellRange.from; i < newVisibleCells.from; i++)
            {
				HideCell(CellRange.FirstCell);
            }
            //Remove cells that disappeared to the bottom
            for (int i = newTo; i < oldTo; i++)
            {
				HideCell(CellRange.LastCell);
            }
            //Add cells that appeared on top
            for (int i = newVisibleCells.from; i < m_visibleCellRange.from; i++) {
				AddCell(i, CellRange.FirstCell);
            }
            //Add cells that appeared on bottom
            for (int i = oldTo + 1; i <= newTo; i++) {
				AddCell(i, CellRange.LastCell);
            }
            m_visibleCellRange = newVisibleCells;
            UpdateScrollRectDynamicFillerElements();
            m_requiresRefresh = false;
        }

        protected abstract void UpdateScrollRectDynamicFillerElements();

        protected void HideCell(CellRange position)
        {
            int cell = position == CellRange.LastCell ? m_visibleCellRange.Last() : m_visibleCellRange.from;

            TableViewCell removedCell = m_visibleCells[cell];
            StoreCellForReuse(removedCell);
            m_visibleCells.Remove(cell);
            m_visibleCellRange.count--;
			if (position == CellRange.FirstCell) {
                m_visibleCellRange.from++;
            }
            this.onCellVisibilityChanged.Invoke(cell, false);
        }

        protected LayoutElement CreateEmptyScrollRectDynamicFillerElement(string name)
        {
            GameObject go = new GameObject(name, typeof(RectTransform), typeof(LayoutElement));
            LayoutElement le = go.GetComponent<LayoutElement>();
            return le;
        }

		protected abstract float CalculateContentHeightToIndex(int index);

		protected abstract float CalculateContentWidthToIndex(int index);

        protected void StoreCellForReuse(TableViewCell cell)
		{
            string reuseIdentifier = cell.reuseIdentifier;
            
            if (string.IsNullOrEmpty(reuseIdentifier)) {
                GameObject.Destroy(cell.gameObject);
                return;
            }

            if (!m_reusableCells.ContainsKey(reuseIdentifier)) {
                m_reusableCells.Add(reuseIdentifier, new LinkedList<TableViewCell>());
            }
            m_reusableCells[reuseIdentifier].AddLast(cell);
            cell.transform.SetParent(m_reusableCellContainer, false);
        }

        #endregion
    }

    internal static class RangeExtensions
    {
        public static int Last(this Range range)
        {
            if (range.count == 0)
            {
                throw new System.InvalidOperationException("Empty range has no to()");
            }
            return (range.from + range.count - 1);
        }

        public static bool Contains(this Range range, int num) {
            return num >= range.from && num < (range.from + range.count);
        }
    }
}
