using UnityEngine;
using System.Collections;
using Tacticsoft;

namespace Tacticsoft.Examples
{
    //An example implementation of a class that communicates with a TableView
    public class HorizontalTableViewController : MonoBehaviour, ITableViewDataSource
    {
        public HorizontalVisibleCounterCell m_cellPrefab;
        public HorizontalTableView m_tableView;

        public int m_numRows;
        private int m_numInstancesCreated = 0;

        //Register as the TableView's delegate (required) and data source (optional)
        //to receive the calls
        void Start() {
			m_tableView.dataSource = this;
        }

        public void SendBeer() {
            Application.OpenURL("https://www.paypal.com/cgi-bin/webscr?business=contact@tacticsoft.net&cmd=_xclick&item_name=Beer%20for%20TSTableView&currency_code=USD&amount=5.00");
        }

        #region ITableViewDataSource

        //Will be called by the TableView to know how many rows are in this table
		public int GetNumberOfCellsForTableView(TableViewBase tableView) {
            return m_numRows;
        }

        //Will be called by the TableView to know what is the size of each index
		public Vector2 GetSizeForCellInTableView(TableViewBase tableView, int index) {
            return new Vector2((m_cellPrefab.transform as RectTransform).rect.width,tableView.TableContentHeight);
        }

        //Will be called by the TableView when a cell needs to be created for display
		public TableViewCell GetCellForIndexInTableView(TableViewBase tableView, int row) {
			HorizontalVisibleCounterCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as HorizontalVisibleCounterCell;
            if (cell == null) {
				cell = (HorizontalVisibleCounterCell)GameObject.Instantiate(m_cellPrefab);
                cell.name = "HorizontalVisibleCounterCellInstance_" + (++m_numInstancesCreated).ToString();
            }
            cell.SetRowNumber(row);
            return cell;
        }

        #endregion

        #region Table View event handlers

        //Will be called by the TableView when a cell's visibility changed
        public void TableViewCellVisibilityChanged(int row, bool isVisible) {
            //Debug.Log(string.Format("Row {0} visibility changed to {1}", row, isVisible));
            if (isVisible) {
				HorizontalVisibleCounterCell cell = (HorizontalVisibleCounterCell)m_tableView.GetCellAtIndex(row);
                cell.NotifyBecameVisible();
            }
        }

        #endregion

    }
}
