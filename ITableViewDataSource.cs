﻿using UnityEngine;
using System.Collections;

namespace Tacticsoft
{
    /// <summary>
    /// An interface for a data source to a TableView
    /// </summary>
	public interface ITableViewDataSource
	{
		/// <summary>
		/// Get the number of cells that a certain table should display
		/// </summary>
		int GetNumberOfCellsForTableView(TableViewBase tableView);
		
		/// <summary>
		/// Get the size of a certain cell in the table view at index
		/// </summary>
		Vector2 GetSizeForCellInTableView(TableViewBase tableView, int index);
		
		/// <summary>
		/// Create a cell for a table view at the given index.
		/// Callers should use tableView.GetReusableCell to cache objects
		/// </summary>
		TableViewCell GetCellForIndexInTableView(TableViewBase tableView, int index);
	}
}

